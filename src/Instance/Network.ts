class Network extends Core {
    private readonly core: Core;
    private readonly uuid: string;
    private readonly name: string;

    constructor(core: Core, instance: Instance) {
        super(core.getTool());
        this.core = core;
        this.uuid = instance.getId();
        this.name = instance.getName();
    }

    public getStore(): Store {
        return new Store(this);
    }

    public getForum(): Forum {
        return new Forum(this);
    }

    public getId(): string {
        return this.uuid;
    }

    public asInstance(): Instance {
        return new Instance(this.core, this.uuid, this.name, "NTW");
    }

    public async createServer(name: string): Promise<Instance> {
        return CoreRequest.getBiStack<Instance>(
            this.core,
            "instance/server/create",
            new ParameterStack()
                .push("network", this.getId())
                .push("name", name),
            new ParameterStack()
                .push("name", name),
            json => new Instance(this.core, json.uuid, json.name, "SVR")
        );
    }

    public async getServers(): Promise<Array<Instance>> {
        return await CoreRequest.get<Array<Instance>>(
            this.core,
            "instance/server/list",
            new ParameterStack()
                .push("network", this.getId()),
            json => json.map((instance: any) => new Instance(this.core, instance.uuid, instance.name, "SVR"))
        );
    }

    public async getVotingAnalytics(span: number = 3600 * 24): Promise<Array<VoteAnalytic>> {
        return CoreRequest.getBiStack<Array<VoteAnalytic>>(
            this.core,
            "instance/network/voting/analytics",
            new ParameterStack()
                .push("network", this.getId())
                .push("span", span.toString()),
            new ParameterStack()
                .push("span", span.toString()),
            json => json.map((analytics: any) => new VoteAnalytic().fromArray(analytics))
        );
    }

    public async getVotingSites(): Promise<Array<VotingSite>> {
        return CoreRequest.getBiStack<Array<VotingSite>>(
            this.core,
            "instance/network/voting/site/list",
            new ParameterStack()
                .push("network", this.getId()),
            new ParameterStack(),
            json => json.map((site: any) => new VotingSite(this.core).fromArray(site))
        );
    }

    public async getSetupVotingSites(displaySetup: boolean = true): Promise<Array<VotingSiteConfig> | Array<VotingSite>> {
        return displaySetup ? CoreRequest.getTriStack<Array<VotingSiteConfig>>(
            this.core,
            "instance/network/voting/site/list/setup/config",
            new ParameterStack()
                .push("network", this.getId()),
            new ParameterStack(),
            new ParameterStack()
                .push("network", this.getId()),
            json => json.map((site: any) => new VotingSiteConfig(this.core).fromArray(site))
        ) : CoreRequest.getTriStack<Array<VotingSite>>(
            this.core,
            "instance/network/voting/site/list/setup",
            new ParameterStack()
                .push("network", this.getId()),
            new ParameterStack(),
            new ParameterStack()
                .push("network", this.getId()),
            json => json.map((site: any) => new VotingSite(this.core).fromArray(site))
        )
    }

    public async getGuild(): Promise<DiscordGuild> {
        return CoreRequest.getBiStack<DiscordGuild>(
            this.core,
            "instance/network/discord/get/guild",
            new ParameterStack()
                .push("network", this.getId()),
            new ParameterStack(),
            json => new DiscordGuild(this).fromArray(json)
        );
    }

    public async setGuild(discordGuildId: string): Promise<boolean> {
        return CoreRequest.get<boolean>(
            this.core,
            "instance/network/discord/setguild",
            new ParameterStack()
                .push("guildid", discordGuildId),
            json => true
        );
    }

    public async setSessionChannel(channelId: string): Promise<boolean> {
        return CoreRequest.get<boolean>(
            this.core,
            "instance/network/discord/setchannel/session",
            new ParameterStack()
                .push("channelid", channelId),
            json => true
        );
    }

    public async setDonationChannel(channelId: string): Promise<boolean> {
        return CoreRequest.get<boolean>(
            this.core,
            "instance/network/discord/setchannel/donation",
            new ParameterStack()
                .push("channelid", channelId),
            json => true
        );
    }

    public async getHashes(): Promise<Array<ConnectionHash>> {
        return CoreRequest.get(
            this.core,
            "session/hash/list",
            new ParameterStack(),
            json => json.map((hash: any) => new ConnectionHash(new Core(this.core.getKey())).fromArray(hash))
        );
    }

    public async getOffences(): Promise<Array<Offence>> {
        return CoreRequest.getTriStack<Array<Offence>>(
            this.core,
            "punishment/offence/list",
            new ParameterStack()
                .push("network", this.getId()),
            new ParameterStack(),
            new ParameterStack()
                .push("network", this.getId()),
            json => json.map((offence: any) => new Offence(this.core).fromArray(offence))
        );
    }

    public async getOffenceActions(): Promise<Array<OffenceAction>> {
        return CoreRequest.get<Array<OffenceAction>>(
            this.core,
            "punishment/action/list",
            new ParameterStack()
                .push("network", this.getId()),
            json => json.map((action: any) => new OffenceAction(this.core).fromArray(action))
        );
    }

    public async searchPlayers(username: string): Promise<Array<Player>> {
        return CoreRequest.getBiStack<Array<Player>>(
            this.core,
            "player/from/minecraft/username/search",
            new ParameterStack()
                .push("network", this.getId())
                .push("username", username),
            new ParameterStack()
                .push("username", username),
            json => json.map((player: any) => new Player(this.core, player.coreid, player.username, player.uuid, player.verified))
        );
    }

    public async getPlayer(coreid: string): Promise<Player> {
        return CoreRequest.get<Player>(
            this.core,
            "player/from/core/id",
            new ParameterStack()
                .push("player", coreid),
            json => new Player(this.core, json.coreid, json.username, json.uuid, json.verified)
        );
    }

    public async getPlayers(page?: number): Promise<Array<Player>> {
        if(page === undefined)
            page = 0;

        return CoreRequest.getBiStack<Array<Player>>(
            this.core,
            "instance/network/list/players",
            new ParameterStack()
                .push("network", this.getId())
                .push("page", page.toString()),
            new ParameterStack()
                .push("page", page.toString()),
            json => json.map((player: any) => new Player(this.core, player.coreid, player.username, player.uuid, player.verified))
        );
    }

    public async getPunishments(page: number = 0): Promise<Array<Punishment>> {
        return CoreRequest.get<Array<Punishment>>(
            this.core,
            "punishment/list",
            new ParameterStack()
                .push("network", this.getId())
                .push("page", page.toString()),
            json => json.map((punishment: any) => new Punishment(this.core).fromArray(punishment))
        );
    }
}
