class CoreRequest {
    private static baseURL: string = 'https://api.purecore.io/rest/2/';

    public static async request<T>(url: string, deserializer: (json: any) => T): Promise<T> {
        const response: Response = await fetch(url);

        if (!response.ok)
            return Promise.reject(new Error(response.statusText));

        const json: T | RequestError = await response.json<T | RequestError>();

        if ('error' in json)
            return Promise.reject(new Error((<RequestError>json).error + ': ' + (<RequestError>json).msg));

        return deserializer.apply(json);
    }

    public static async get<T>(core: Core, endpoint: string, stack: ParameterStack, deserializer: (json: any) => T): Promise<T> {
        return this.request<T>(this.baseURL + this.formatEndpoint(endpoint) + '?' +
            stack.pushTool(core).format(), deserializer);
    }

    public static async getBiStack<T>(core: Core, endpoint: string, hashStack: ParameterStack, keyStack: ParameterStack, deserializer: (json: any) => T): Promise<T> {
        return this.get<T>(core, endpoint, typeof core.getTool() === 'string' ? keyStack : hashStack, deserializer);
    }

    public static async getTriStack<T>(core: Core, endpoint: string, hashStack: ParameterStack, keyStack: ParameterStack, emptyStack: ParameterStack, deserializer: (json: any) => T): Promise<T> {
        return this.get<T>(core, endpoint, core.getTool() === null ? emptyStack : typeof core.getTool() === 'string' ? keyStack : hashStack, deserializer);
    }

    private static formatEndpoint(endpoint: string): string {
        return (endpoint.startsWith('/') ? '' : '/') + endpoint + (endpoint.endsWith('/') ? '' : '/')
    }
}
