class ParameterStack {
    private parameters: Map<string, string>;

    constructor() {
        this.parameters = new Map<string, string>();
    }

    public push(key: string, value: string): ParameterStack {
        this.parameters.set(key, value);
        return this;
    }

    public pushTool(core: Core): ParameterStack {
        if(core.getTool() != null)
            this.push(typeof core.getTool() === 'string' ? 'key' : 'hash', typeof core.getTool() === 'string' ?
                core.getKey() : core.getCoreSession().getHash())

        return this;
    }

    public format(): string {
        return Array.from(this.parameters.entries())
            .map((parameter: [string, string]) => parameter[0] + "=" + parameter[1])
            .join("&")
    }
}
