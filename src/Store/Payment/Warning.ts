class Warning {

    public cause: string;
    public text: string;

    constructor(cause: string, text: string){
        this.cause=cause;
        this.text=text;
    }

}