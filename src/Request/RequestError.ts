interface RequestError {
    error: string
    msg: string
}
